<?php
class Krug {
  public $r;
  private $pi = 3.14159265; 

  function __construct($r) {
    $this->r = $r;
  } 
  
  function get_povrsina() {
         return   $this->r ** 2 * $this->pi;
    }
   function get_opseg() {
        return  2 *  $this->r * $this->pi;
    }
}

class Trokut  {
	
  public $a;   
  public $va; //visina na a
  public $b;   
  public $c;   
  
  function __construct($a, $va, $b, $c ) {
    $this->a = $a;
    $this->va = $va;
    $this->b = $b;
    $this->c = $c;
  } 
  
  function get_povrsina() {
        return $this->a * $this->va /2;
    }
   function get_opseg() {
        return  $this->a + $this->b + $this->c;
    }
}

$krug1 = new Krug(10);
echo "krug r=10 -> P=" . $krug1->get_povrsina() . ", O=" . $krug1->get_opseg() . "\r\n";

$trokut1 = new trokut(5, 2.4 , 3, 4);
echo "trokut a=5 va=2.4 b=3 c=4 -> P=" . $trokut1->get_povrsina() . ", O=" . $trokut1->get_opseg(). "\r\n";
?> 